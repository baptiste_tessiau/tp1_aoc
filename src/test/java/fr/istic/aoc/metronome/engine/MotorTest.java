package fr.istic.aoc.metronome.engine;

import fr.istic.aoc.metronome.command.ICommand;
import fr.istic.aoc.metronome.command.TypeEvt;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;

public class MotorTest {

    private Motor motor;

    @Before
    public void setUp(){
        motor = new Motor();
    }

    @After
    public void tearDown(){
        motor = null;
    }

    @Test
    public void testMotor(){
        assertFalse(motor.getEnMarche());
        assertEquals(4, motor.getBar());
        assertEquals(20, motor.getBpm());
    }

    @Test(expected=NullPointerException.class)
    public void testSetEnMarche(){
        assertFalse(motor.getEnMarche());
        motor.setEnMarche(true);
    }

    @Test
    public void testSetEnMarche2(){
        assertFalse(motor.getEnMarche());
        motor.setCommand(TypeEvt.UpdateEnMarche, () -> {});
        motor.setCommand(TypeEvt.UpdateBpm, () -> {});
        motor.setCommand(TypeEvt.UpdateBar, () -> {});
        motor.setEnMarche(true);
        assertTrue(motor.getEnMarche());
    }

    @Test
    public void testSetEnMarche3(){
        assertFalse(motor.getEnMarche());
        motor.setCommand(TypeEvt.UpdateEnMarche, () -> {});
        motor.setCommand(TypeEvt.UpdateBpm, () -> {});
        motor.setCommand(TypeEvt.UpdateBar, () -> {});
        motor.setEnMarche(false);
        assertFalse(motor.getEnMarche());
    }

    @Test
    public void testSetBpm(){
        motor.setCommand(TypeEvt.UpdateEnMarche, () -> {});
        motor.setCommand(TypeEvt.UpdateBpm, () -> {});
        motor.setCommand(TypeEvt.UpdateBar, () -> {});
        motor.setBpm(20);
        assertEquals(20, motor.getBpm());
        motor.setBar(2);
        assertEquals(2, motor.getBar());
        motor.setEnMarche(true);
        motor.setBpm(300);
        assertEquals(300, motor.getBpm());
        motor.setBar(7);
        assertEquals(7, motor.getBar());
        motor.setEnMarche(false);
    }

    @Test
    public void testTick(){
        ICommand bpm = Mockito.mock(ICommand.class);
        ICommand bar = Mockito.mock(ICommand.class);
        motor.setCommand(TypeEvt.MarquerMesure, bar);
        motor.setCommand(TypeEvt.MarquerTemps, bpm);
        for (int i = 0; i < 4; i++) {
            motor.tick();
        }
        Mockito.verify(bar).execute();
        Mockito.verify(bpm, Mockito.times(4)).execute();
    }
}
