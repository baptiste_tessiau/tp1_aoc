package fr.istic.aoc.metronome.engine;

import fr.istic.aoc.metronome.command.ICommand;
import fr.istic.aoc.metronome.command.TypeEvt;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by btessiau on 23/10/15.
 */
public class Motor implements IMotor {

    /**
     * Mesure du moteur
     */
    private int bar;

    /**
     * Tempo du moteur
     */
    private int bpm;

    /**
     * État du moteur
     */
    private boolean enMarche;

    /**
     * Générateur d'évènement périodique correspondant au tempo, activé quand enMarche == true et paramètré par bpm
     */
    private IClock clock;

    /**
     * Attribut permettant de lever un évènement correspondant à la mesure
     */
    private int countTick = 0;

    /**
     * Commandes utilisées dans le cadre du PC command
     */
    private Map<TypeEvt,ICommand> commands;
    
    public Motor() {
        enMarche = false;
        bar = 4;
        bpm = 20;
        clock = new Clock();
        commands = new HashMap<TypeEvt,ICommand>();

        clock.setCommand(TypeEvt.Tick, generateCommand(TypeEvt.Tick));
    }

    @Override
    public void setCommand(TypeEvt typeEvt, ICommand command) {
            commands.put(typeEvt, command);

    }

    @Override
    public int getBar() {
        return bar;
    }

    @Override
    public void setBar(int bar) {
        this.bar = bar;
        countTick = 0;
        commands.get(TypeEvt.UpdateBar).execute();
    }

    @Override
    public int getBpm() {
        return bpm;
    }

    @Override
    public void setBpm(int bpm) {
        this.bpm = bpm;
        countTick = 0;
        commands.get(TypeEvt.UpdateBpm).execute();
        valueChanged();
    }

    @Override
    public boolean getEnMarche() {
        return enMarche;
    }

    @Override
    public void setEnMarche(boolean enMarche) {
        this.enMarche = enMarche;
        commands.get(TypeEvt.UpdateEnMarche).execute();
        commands.get(TypeEvt.UpdateBpm).execute();
        commands.get(TypeEvt.UpdateBar).execute();

        if(enMarche) {
            clock.startClock(bpm);
        } else {
            clock.stopClock();
        }
    }

    @Override
    public void tick() {
        commands.get(TypeEvt.MarquerTemps).execute();
        if ((++countTick)%bar == 0){
            countTick=0;
            commands.get(TypeEvt.MarquerMesure).execute();
        }

    }

    /**
     * génère la commande utilisé par clock périodiquement
     * @param typeEvt type de l'évènement
     * @return
     */
    private ICommand generateCommand(TypeEvt typeEvt) {
        switch (typeEvt) {
            case Tick:
                return () -> {
                    this.tick();
                };
            default:
                return null;
        }
    }

    /**
     * Lors de la modification du tempo
     */
    private void valueChanged() {
        if (enMarche) {
            clock.stopClock();
            clock.startClock(bpm);
        }

    }
}
