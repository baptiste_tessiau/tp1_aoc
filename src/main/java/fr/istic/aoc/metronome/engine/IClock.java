package fr.istic.aoc.metronome.engine;

import fr.istic.aoc.metronome.command.ICommand;
import fr.istic.aoc.metronome.command.TypeEvt;

/**
 * The {@link IClock} interface gives access to a set of methods on a specific implementation
 * to start, stop and gives commands that will be executed at a specific rate.
 */
public interface IClock {

    /**
     * Starts the clock with a frequency of 60/bpm.
     * @param bpm the number of tick per minute the clock should produce
     */
    void startClock(int bpm);

    /**
     * Stops the clock.
     */
    void stopClock();

    /**
     * Sets the {@link ICommand command} to a specific {@link TypeEvt event}
     * @param typeEvt the type of event for which you want to set a command
     * @param command the command to set
     */
    void setCommand(TypeEvt typeEvt, ICommand command);
}
