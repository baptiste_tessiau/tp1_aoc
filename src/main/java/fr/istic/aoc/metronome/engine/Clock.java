package fr.istic.aoc.metronome.engine;

import fr.istic.aoc.metronome.command.ICommand;
import fr.istic.aoc.metronome.command.TypeEvt;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Clock implements IClock {

    /**
     * A service that will hold the thread of the scheduled task.
     */
    private ScheduledExecutorService s;
    /**
     * The map of {@link ICommand commands} link to the corresponding {@link TypeEvt event}.
     */
    private Map<TypeEvt,ICommand> commands;

    public Clock(){
        s = Executors.newScheduledThreadPool(1);
        commands = new HashMap<TypeEvt,ICommand>();
    }

    @Override
    public void setCommand(TypeEvt typeEvt, ICommand command) {
        commands.put(typeEvt, command);
    }

    @Override
    public void startClock(int bpm) {
            s.scheduleAtFixedRate(() -> tick(), 0, (60  * 1000) / bpm, TimeUnit.MILLISECONDS);
    }

    @Override
    public void stopClock() {
        s.shutdownNow();
        s = Executors.newScheduledThreadPool(1);
    }

    private void tick(){
        commands.get(TypeEvt.Tick).execute();
    }

}
