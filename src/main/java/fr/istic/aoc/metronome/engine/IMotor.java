package fr.istic.aoc.metronome.engine;

import fr.istic.aoc.metronome.command.ICommand;
import fr.istic.aoc.metronome.command.TypeEvt;

/**
 * Created by btessiau on 23/10/15.
 */
public interface IMotor {

    /**
     * Ajoute une commande dans le cadre du PC Command
     * @param typeEvt type de la command
     * @param command command
     */
    void setCommand(TypeEvt typeEvt, ICommand command);

    /**
     *
     * @return la mesure entre 2 et 7
     */
    int getBar();

    /**
     *
     * @return le tempo entre 20 et 300
     */
    int getBpm();

    /**
     *
     * @return true ssi le moteur est en marche
     */
    boolean getEnMarche();

    /**
     *
     * @param bar compris entre 2 et 7
     */
    void setBar(int bar);

    /**
     *
     * @param bpm compris entre 20 et 300
     */
    void setBpm(int bpm);

    /**
     * modifie l'état du moteur
     * @param enMarche true ssi le moteur est en marche
     */
    void setEnMarche(boolean enMarche);

    /**
     * Méthode qui va actionner les commandes liées aux évènements marquerTemps et marquerMesure
     */
    void tick();

}
