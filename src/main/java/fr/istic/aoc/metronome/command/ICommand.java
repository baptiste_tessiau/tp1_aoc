package fr.istic.aoc.metronome.command;

/**
 * Created by btessiau on 23/10/15.
 */
public interface ICommand {

    /**
     * methode du PC command
     */
    void execute();
}
