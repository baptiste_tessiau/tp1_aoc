package fr.istic.aoc.metronome.command;

/**
 * Evenement servant à indiquer le but des commandes à utilise
 * Created by btessiau on 23/10/15.
 */
public enum TypeEvt {
    MarquerTemps, MarquerMesure, UpdateBpm, UpdateBar, UpdateEnMarche ,
    Start, Stop, Inc, Dec, Tick, TickbarLed, TickbpmLed, ChangeBpmDisplay, ChangedBpmSlider
}
