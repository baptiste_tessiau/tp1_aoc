package fr.istic.aoc.metronome;

import fr.istic.aoc.metronome.controller.Controller;
import fr.istic.aoc.metronome.controller.IController;
import fr.istic.aoc.metronome.view.MetronomeView;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Metronome App
 *
 */
public class MetronomeApp extends Application {

    public static void main(String[] args) {
        // launch JavaFX application
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        Parent root = loader.load(getClass().getResource("/metronome.fxml").openStream());

        MetronomeView metronomeView = loader.getController();
        IController controller = new Controller();
        controller.setMetronomeView(metronomeView);
        controller.init();
        stage.setTitle("Metronome");

        Scene scene = new Scene(root,600, 400);
        scene.getStylesheets().add("metronome.css");
        scene.getStylesheets().add("VolumeSlider.css");
        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void stop() throws Exception {
        System.out.println("quit");
        System.exit(0);
    }
}
