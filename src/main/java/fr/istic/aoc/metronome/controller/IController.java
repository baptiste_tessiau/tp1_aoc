package fr.istic.aoc.metronome.controller;

import fr.istic.aoc.metronome.view.MetronomeView;

/**
 * Created by btessiau on 23/10/15.
 */
public interface IController {

    /**
     * Remonte via une commande l'évènement lié au tempo
     */
    void marquerTemps();

    /**
     * Remonte via une commande l'évènement lié à la mesure
     */
    void marquerMesure();

    /**
     * Remonte via une commande l'évènement qui indique que le tempo a changé
     */
    void updateBpm();

    /**
     * Remonte via une commande l'évènement qui indique que la mesure a changé
     */
    void updateBar();

    /**
     * Remonte via une commande l'évènement qui indique que le moteur est en marche
     */
    void updateEnMarche();

    /**
     * Active via une commande le moteur
     */
    void start();

    /**
     * Désactive via une commande le moteur
     */
    void stop();

    /**
     * Incrémente la mesure
     */
    void inc();

    /**
     * Décrémente la mesure
     */
    void dec();

    /**
     * Modifie le tempo
     */
    void changedBpmSlider();

    /**
     * Modifie la le vue du controller
     */
    void setMetronomeView(MetronomeView metronomeView);

    /**
     * Initialisation du controller
     */
    void init();
}
