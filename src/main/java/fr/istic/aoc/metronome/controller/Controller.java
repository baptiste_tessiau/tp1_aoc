package fr.istic.aoc.metronome.controller;

import fr.istic.aoc.metronome.command.ICommand;
import fr.istic.aoc.metronome.command.TypeEvt;
import fr.istic.aoc.metronome.engine.IMotor;
import fr.istic.aoc.metronome.engine.Motor;
import fr.istic.aoc.metronome.view.MetronomeView;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by btessiau on 23/10/15.
 */
public class Controller implements IController {

    /**
     * boolean correspondant à l'état du moteur
     */
    Boolean enMarche;

    /**
     * moteur du controller
     */
    IMotor motor;

    /**
     * vue du controller
     */
    MetronomeView metronomeView;

    /**
     * commands du controller dans le cadre du PC command avec le moteur et la vue
     */
    private Map<TypeEvt,ICommand> commands = new HashMap<>();

    /**
     * tempo correspondant au tempo du moteur
     */
    private int bpm;

    /**
     * mesure correspondante à la mesure du moteur
     */
    private int bar;

    @Override
    public void marquerTemps() {
        commands.get(TypeEvt.TickbpmLed).execute();

    }

    @Override
    public void marquerMesure() {
        commands.get(TypeEvt.TickbarLed).execute();

    }

    @Override
    public void init() {
        motor = new Motor();

        this.enMarche = false;
        this.bar = motor.getBar();
        this.bpm = motor.getBpm();

        motor.setCommand(TypeEvt.MarquerMesure, generateCommand(TypeEvt.MarquerMesure));
        motor.setCommand(TypeEvt.MarquerTemps, generateCommand(TypeEvt.MarquerTemps));
        motor.setCommand(TypeEvt.UpdateBpm, generateCommand(TypeEvt.UpdateBpm));
        motor.setCommand(TypeEvt.UpdateBar, generateCommand(TypeEvt.UpdateBar));
        motor.setCommand(TypeEvt.UpdateEnMarche, generateCommand(TypeEvt.UpdateEnMarche));

        metronomeView.setCommand(TypeEvt.Start, generateCommand(TypeEvt.Start));
        metronomeView.setCommand(TypeEvt.Stop, generateCommand(TypeEvt.Stop));
        metronomeView.setCommand(TypeEvt.Inc, generateCommand(TypeEvt.Inc));
        metronomeView.setCommand(TypeEvt.Dec, generateCommand(TypeEvt.Dec));
        metronomeView.setCommand(TypeEvt.ChangedBpmSlider, generateCommand(TypeEvt.ChangedBpmSlider));

        commands.putAll(metronomeView.getCommands());
    }

    @Override
    public void setMetronomeView(MetronomeView metronomeView) {
        this.metronomeView = metronomeView;
    }

    @Override
    public void updateBpm() {
        bpm = motor.getBpm();
        metronomeView.setBar(bar);
        metronomeView.setBpm(bpm);
        commands.get(TypeEvt.ChangeBpmDisplay).execute();
    }

    @Override
    public void updateBar() {
        bar = motor.getBar();
        metronomeView.setBar(bar);
        metronomeView.setBpm(bpm);
        commands.get(TypeEvt.ChangeBpmDisplay).execute();
    }

    @Override
    public void updateEnMarche() {
        enMarche = motor.getEnMarche();
    }

    @Override
    public void start() {
        if (!enMarche) {
            motor.setEnMarche(true);
        }
    }

    @Override
    public void stop() {
        if (enMarche) {
            motor.setEnMarche(false);
        }
    }

    @Override
    public void inc() {
        int newBar = motor.getBar() + 1;
        if (newBar < 8) {
            motor.setBar(newBar);
        }
    }

    @Override
    public void dec() {
        int newBar = motor.getBar() - 1;
        if (newBar > 1) {
            motor.setBar(newBar);
        }

    }

    @Override
    public void changedBpmSlider() {
        int t = ((int) metronomeView.getBpmSliderPosition());
        if (t >= 20 & t <= 300) {
            motor.setBpm(t);
        }
        else if (t < 20 ) {
            motor.setBpm(20);
        }
        else {
            motor.setBpm(300);
        }
    }

    /**
     * Génère les commandes à donner à la vue et au moteur
     * @param typeEvt
     * @return
     */
    private ICommand generateCommand(TypeEvt typeEvt) {
        switch (typeEvt) {
            case MarquerMesure:
                return () -> {this.marquerMesure();};
            case MarquerTemps:
                return () -> {this.marquerTemps();};
            case UpdateBpm:
                return () -> {this.updateBpm();};
            case UpdateBar:
                return () -> {this.updateBar();};
            case UpdateEnMarche:
                return () -> {this.updateEnMarche();};
            case Start:
                return () -> {this.start();};
            case Stop:
                return () -> {this.stop();};
            case Inc:
                return () -> {this.inc();};
            case Dec:
                return () -> {this.dec();};
            case ChangedBpmSlider:
                return () -> {this.changedBpmSlider();};
            default:
                return null;
        }
    }
}
