package fr.istic.aoc.metronome.view;

import eu.hansolo.enzo.lcd.Lcd;
import eu.hansolo.enzo.led.Led;
import fr.istic.aoc.metronome.command.ICommand;
import fr.istic.aoc.metronome.command.TypeEvt;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.media.AudioClip;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 *
 * Created by leiko on 23/10/15.
 */
public class MetronomeViewImpl implements MetronomeView, Initializable {
    /**
     * The delay during which a {@link Led} should stay on.
     */
    private int persistTickLed;
    /**
     * The {@link AudioClip} played each time the {@link MetronomeViewImpl#bpmLed} is set on.
     */
    private AudioClip soundSnare;
    /**
     * The {@link AudioClip} played each time the {@link MetronomeViewImpl#barLed} is set on.
     */
    private AudioClip soundKick;
    /**
     * The number of beat per bar
     */
    private int bar;
    /**
     * The number of beat per minute
     */
    private int bpm;

    /**
     * The {@link Lcd} component used to display current BPM.
     */
    @FXML
    private Lcd bpmDisplay;
    /**
     * The {@link Led bpmLed} will produce a tick every times the view is told to.
     * Its tick represents the BPM.
     */
    @FXML
    private Led bpmLed;
    /**
     * The {@link Led barLed} will produce a tick every times the view is told to.
     * Its tick represents the start of the bar that can contain many beats.
     */
    @FXML
    private Led barLed;
    /**
     * The {@link Button startButton} will start the metronome.
     */
    @FXML
    private Button startButton;
    /**
     * The {@link Button stopButton} will stop the metronome.
     */
    @FXML
    private Button stopButton;
    /**
     * The {@link Button incButton} will increase the number of beat per bar.
     */
    @FXML
    private Button incButton;
    /**
     * The {@link Button decButton} will decrease the number of beat per bar.
     */
    @FXML
    private Button decButton;
    /**
     * The {@link VolumeButton tempo} is a circular slider.
     * It is used to set the tempo of the metronome.
     */
    @FXML
    private VolumeButton tempo;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        persistTickLed = 100;

        bpmDisplay.getStyleClass().add(Lcd.STYLE_CLASS_BLACK_RED);
        bpmDisplay.setTextMode(true);

    }

    @Override
    public void setCommand(TypeEvt typeEvt, ICommand command) {
        switch (typeEvt) {
            case Start:
                startButton.setOnAction((e) -> command.execute());
                break;
            case Stop:
                stopButton.setOnAction((e) -> command.execute());
                break;
            case Inc:
                incButton.setOnAction((e) -> command.execute());
                break;
            case Dec:
                decButton.setOnAction((e) -> command.execute());
                break;
            case ChangedBpmSlider:
                tempo.setOnMouseReleased(e -> command.execute());
                break;
        }

    }

    private ICommand generateCommand(TypeEvt typeEvt) {
        switch (typeEvt) {
            case TickbarLed:
                return () -> {tickbarLed();};
            case TickbpmLed:
                return () -> {tickbpmLed();};
            case ChangeBpmDisplay:
                return () -> {setBpmDisplay();};
            default:
                return null;
        }
    }

    private ScheduledExecutorService es = Executors.newScheduledThreadPool(1);

    private void tickbarLed() {
        Runnable r = () -> barLed.setOn(false);

        es.shutdown();
        es = Executors.newScheduledThreadPool(1);

        es.schedule(
                r,
                this.persistTickLed,
                TimeUnit.MILLISECONDS
        );

        barLed.setOn(true);
        playSoundSnare();
    }

    private ScheduledExecutorService ess = Executors.newScheduledThreadPool(1);
    private void tickbpmLed() {
        Runnable r = () -> bpmLed.setOn(false);

        ess.shutdown();
        ess = Executors.newScheduledThreadPool(1);
        ess.schedule(
                r,
                this.persistTickLed,
                TimeUnit.MILLISECONDS
        );

        bpmLed.setOn(true);
        playSoundKick();
    }

    private void setBpmDisplay() {
        bpmDisplay.setText("Bpm : " + String.valueOf(bpm));
    }

    @Override
    public void setBar(int bar) {
        this.bar = bar;
    }

    @Override
    public void setBpm(int bpm) {
        this.bpm = bpm;
    }

    @Override
    public double getBpmSliderPosition() {
        return tempo.getValue();

    }

    private void playSoundSnare () {
        if (soundSnare != null) {
            soundSnare.stop();
        } else {
            Class cc = this.getClass();
            URL ss = cc.getResource("/snare.wav");
            String str = ss.toString();
            soundSnare = new AudioClip(str);
        }
        soundSnare.play();

    }

    private void playSoundKick() {
        if (soundKick != null) {
            soundKick.stop();
        } else {
            Class cc = this.getClass();
            URL ss = cc.getResource("/kick.wav");
            String str = ss.toString();
            soundKick = new AudioClip(str);
        }
        soundKick.play();

    }


    @Override
    public Map<TypeEvt,ICommand> getCommands(){

        Map<TypeEvt,ICommand> commands = new HashMap<>();
        commands.put(TypeEvt.TickbarLed, generateCommand(TypeEvt.TickbarLed));
        commands.put(TypeEvt.TickbpmLed, generateCommand(TypeEvt.TickbpmLed));
        commands.put(TypeEvt.ChangeBpmDisplay, generateCommand(TypeEvt.ChangeBpmDisplay));
        return commands;
    }

}
