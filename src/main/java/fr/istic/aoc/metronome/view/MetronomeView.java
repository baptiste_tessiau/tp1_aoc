package fr.istic.aoc.metronome.view;

import fr.istic.aoc.metronome.command.ICommand;
import fr.istic.aoc.metronome.command.TypeEvt;

import java.util.Map;

public interface MetronomeView {

    /**
     * Méthode permettant l'initialisation des commandes
     * @param typeEvt not null
     * @param command not null
     */
    void setCommand(TypeEvt typeEvt, ICommand command);

    /**
     * Retourne les commandes qui seront utilisée par le controller
     * @return
     */
    Map<TypeEvt,ICommand> getCommands();

    /**
     * Retourne la valeur du slider
     * @return bpm compris entre 20 et 300
     */
    double getBpmSliderPosition();

    /**
     *
     * @param bar
     */
    void setBar(int bar);

    /**
     * Modifie la valeur du bpm à afficher par la vue
     * @param bpm
     */
    void setBpm(int bpm);
}
