package fr.istic.aoc.metronome.view;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Region;
import javafx.scene.layout.RegionBuilder;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;

public class VolumeButton extends Region {

    private Region knob = RegionBuilder.create().id("knob").build(); // NOI18N.

    private Region overlay = RegionBuilder.create().id("overlay").build(); // NOI18N.
    private final double minAngle = -20;
    private final double maxAngle = 200;
    private Rotate rotate = new Rotate();
    private Line currentLine = new Line();
    private Line minLine = new Line();
    private Line maxLine = new Line();

    public VolumeButton() {
        super();
        knob.setPrefSize(75, 75);
        knob.getStyleClass().add("knob"); // NOI18N.
        knob.getTransforms().add(rotate);
        overlay.setPrefSize(75, 75);
        overlay.getStyleClass().add("overlay");
        overlay.getTransforms().add(rotate);


        // StackPane to add an overlay to the knob
        StackPane stackPane = new StackPane();
        stackPane.setPrefSize(100, 100);
        stackPane.setMaxSize(100, 100);
        stackPane.getStyleClass().add("volume-slider");
        StackPane.setMargin(knob, new Insets(12.5, 12.5, 12.5, 12.5));
        StackPane.setMargin(overlay, new Insets(12.5, 12.5, 12.5, 12.5));
        Text minText = new Text(min.getValue().intValue()+"");
        StackPane.setAlignment(minText, Pos.BOTTOM_LEFT);
        Text maxText = new Text(max.getValue().intValue()+"");
        StackPane.setAlignment(maxText, Pos.BOTTOM_RIGHT);
        stackPane.getChildren().addAll(minText, maxText);
        StackPane.setAlignment(knob, Pos.CENTER);
        StackPane.setAlignment(overlay, Pos.CENTER);

        stackPane.getChildren().addAll(knob, overlay);
        getChildren().add(stackPane);

        setOnMouseMoved(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                double x = event.getX();
                double y = event.getY();
                double centerX = getWidth() / 2.0;
                double centerY = getHeight() / 2.0;
                currentLine.setStartX(centerX);
                currentLine.setStartY(centerY);
                currentLine.setEndX(x);
                currentLine.setEndY(y);
            }
        });
        setOnMouseDragged(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                double x = event.getX();
                double y = event.getY();
                double centerX = getWidth() / 2.0;
                double centerY = getHeight() / 2.0;
                currentLine.setStartX(centerX);
                currentLine.setStartY(centerY);
                currentLine.setEndX(x);
                currentLine.setEndY(y);
                double theta = Math.atan2((y - centerY), (x - centerX));
                double angle = Math.toDegrees(theta);

                if (angle > 0.0) {
                    angle = 180 + (180 - angle);
                } else {
                    angle = 180 - (180 - Math.abs(angle));
                }
                if (angle >= 270) {
                    angle =  angle - 360;
                }

                double value = angleToValue(angle);
                setValue(value);
            }
        });


        setPrefSize(100, 100);

        valueProperty().addListener((o,l,d) -> requestLayout());
        minProperty().addListener((o,l,d) -> requestLayout());
        maxProperty().addListener((o,l,d) -> requestLayout());

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void layoutChildren() {
        super.layoutChildren();
        double centerX = getWidth() / 2.0;
        double centerY = getHeight() / 2.0;
        currentLine.setStartX(centerX);
        currentLine.setStartY(centerY);
        minLine.setStartX(centerX);
        minLine.setStartY(centerY);
        minLine.setEndX(centerX + 90 * Math.cos(Math.toRadians(-minAngle)));
        minLine.setEndY(centerY + 90 * Math.sin(Math.toRadians(-minAngle)));
        maxLine.setStartX(centerX);
        maxLine.setStartY(centerY);
        maxLine.setEndX(centerX + 90 * Math.cos(Math.toRadians(-maxAngle)));
        maxLine.setEndY(centerY + 90 * Math.sin(Math.toRadians(-maxAngle)));
        double knobX = (getWidth() - knob.getPrefWidth()) / 2.0;
        double knobY = (getHeight() - knob.getPrefHeight()) / 2.0;
        knob.setLayoutX(knobX);
        knob.setLayoutY(knobY);
        double value = getValue();
        double angle = valueToAngle(getValue());
        /*if (minAngle <= angle && angle <= maxAngle) {
            rotate.setPivotX(knob.getWidth() / 2.0);
            rotate.setPivotY(knob.getHeight() / 2.0);
            rotate.setAngle(-angle+90.0);
        }*/
        if (minAngle <= angle && angle <= maxAngle) {
            rotate.setPivotX(knob.getPrefWidth() / 2.0);
            rotate.setPivotY(knob.getPrefHeight() / 2.0);
            rotate.setAngle(-angle+90.0);
        }
    }

    double valueToAngle(double value) {
        double maxValue = getMax();
        double minValue = getMin();
        double angle = minAngle + (maxAngle - minAngle) * (maxValue - value) / (maxValue - minValue);
        return angle;
    }

    double angleToValue(double angle) {
        double maxValue = getMax();
        double minValue = getMin();
        double value = maxValue - (maxValue - minValue) * (angle - minAngle) / (maxAngle - minAngle);
        value = Math.max(minValue, value);
        value = Math.min(maxValue, value);
        return value;
    }
    //
    private final DoubleProperty value = new SimpleDoubleProperty(this, "value", 20); // NOI18N.

    public final void setValue(double v) {
        value.set(v);
    }

    public final double getValue() {
        return value.get();
    }

    public final DoubleProperty valueProperty() {
        return value;
    }
    private final DoubleProperty min = new SimpleDoubleProperty(this, "min", 20); // NOI18N.

    public final void setMin(double v) {
        min.set(v);
    }

    public final double getMin() {
        return min.get();
    }

    public final DoubleProperty minProperty() {
        return min;
    }
    private final DoubleProperty max = new SimpleDoubleProperty(this, "max", 300); // NOI18N.

    public final void setMax(double v) {
        max.set(v);
    }

    public final double getMax() {
        return max.get();
    }

    public final DoubleProperty maxProperty() {
        return max;
    }
}